# Tax Calculation
This program is used to calculate the tax for products in a store.

## Table of Contents
- [Introduction](#introduction)
- [Technologies](#technologies)
- [Setup](#setup)

## Introduction
 A store needs to calculate and apply basic sales tax for the products sold. Sales tax is applied as standard 10% for all items, except Books, Food and Medical products.
 An additional 5% import duty is levied on all imported products, with no exceptions.

### Product class
 This class is used to store the products in the Product Object. This class has the variables quantity, price and name.
 
### Store Class
 This class is used to Calcuate the Tax based on the product type.The products are read from the user and stored in an arraylist. Then the **calculateTotalTax** is used to calculate the tax of the list of products.

## Technologies
In this, the following tools/technologies are used

1. Java 11
2. Junit 5.8.1
3. Lombok

## Setup
 To run the program, the system should be installed with Java.The libraries such as Junit and Lombok should be installed and added into the jar files.

