package StoreReceiptPrint;

import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <h1>Store</h1>
 * This Store class is used to implement the Department store billing process.
 */
public class Store {
    private static final Logger logger = Logger.getLogger(Store.class.getName());
    List<Product> products;

    Store() {
        products = new ArrayList<>();
    }

    /**
     * This is the main method of the class which initialises and call all the process involved.
     *
     * @param args Unused.
     * @throws throw exception on input mismatch.
     */
    public static void main(String[] args) throws Exception {
        try {
            Scanner readInput = new Scanner(System.in);
            Store store = new Store();
            readProducts(readInput, store);
            double tax = calculateTotalTax(store);
            double totalAmount = calculateTotalAmount(store);
            printAllProducts(store);
            logger.info("Tax : " + tax);
            logger.info("Total : " + totalAmount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This adds the product given by the user into the products list.
     *
     * @param readInput Used to read the data from the Standard Input Stream
     * @param store     Store object used to access the products list
     * @return
     */
    public static void readProducts(Scanner readInput, Store store) {
        while (true) {
            String lineValue = readInput.nextLine();
            if (lineValue.equals("")) {
                return;
            }
            String product[] = lineValue.split(": ");
            if (product.length != 3)
                return;
            store.products.add(new Product(product[0], Integer.parseInt(product[1].trim()), Double.parseDouble(product[2].trim())));
        }
    }

    /**
     * <h1>Calculate Total Tax</h1>
     * This calculates the total tax of the product with the condition for the tax calculation.
     *
     * @param store Store object used to access the products list
     * @return double This returns the total calculated tax value of all the products.
     */
    public static double calculateTotalTax(Store store) {
        double totalTax = 0.0d;
        int importedBookChocolateMedicineTaxPercent = 5, importedOtherProductsTaxPercent = 15, NormalTaxPercent = 10;
        for (Product product : store.products) {
            if (isImported(product.getName())) {
                if (product.getName().toLowerCase().contains("book") || product.getName().toLowerCase().contains("chocolate") || product.getName().toLowerCase().contains("medicine")) {
                    totalTax += (roundToTwoDecimal(calculateTaxOfProduct(product, importedBookChocolateMedicineTaxPercent)) * product.getQuantity());
                } else {
                    totalTax += (roundToTwoDecimal(calculateTaxOfProduct(product, importedOtherProductsTaxPercent)) * product.getQuantity());
                }
            } else {
                if (!product.getName().toLowerCase().contains("book") && !product.getName().toLowerCase().contains("chocolate") && !product.getName().toLowerCase().contains("medicine"))
                    totalTax += (roundToTwoDecimal(calculateTaxOfProduct(product, NormalTaxPercent)) * product.getQuantity());
            }
        }
        return totalTax;
    }

    /**
     * Checks if the product is imported or not.
     *
     * @param name Name of the Product
     * @return boolean
     */
    public static boolean isImported(String name) {
        return name.toLowerCase().contains("imported");
    }

    /**
     * <h1>Calculate tax of single product</h1>
     * This method is used to calculate the tax of a single product.
     *
     * @param product    contains the details of the product
     * @param taxPercent The tax percent of the product
     * @return double This returns the tax of a single product
     */
    public static double calculateTaxOfProduct(Product product, int taxPercent) throws NullPointerException {
        if (product == null) {
            throw new NullPointerException("Product object cannot be null while calculating tax");
        }
        double taxOfProduct = (product.getPrice() * taxPercent) / 100;
        product.setPrice(roundToTwoDecimal(product.getPrice() + taxOfProduct));
        return taxOfProduct;
    }

    /**
     * <h1>Round to Two Decimal</h1>
     * This method rounds the decimal values to two places.
     *
     * @param decimal Decimal value of the price
     * @return double
     */
    public static double roundToTwoDecimal(double decimal) {
        return Math.round(decimal * 20.0) / 20.0;
    }

    /**
     * <h1>Calculate Tax Amount</h1>
     * This method calculates the total amount of the total products including taxes.
     *
     * @param store Store object used to access the product list.
     * @return double
     */
    public static double calculateTotalAmount(Store store) {
        double totalAmount = 0.0d;
        for (Product product : store.products) {
            totalAmount += (product.getPrice() * product.getQuantity());
        }
        return roundToTwoDecimal(totalAmount);
    }

    /**
     * This method prints the products including the quantity and price
     *
     * @param store Store object used to access the product list.
     * @return nothing
     */
    public static void printAllProducts(Store store) {
        for (Product product : store.products) {
            System.out.println(product.getName() + " : " + product.getQuantity() + " : " + product.getPrice() * product.getQuantity());
        }
    }

}
