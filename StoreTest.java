package StoreReceiptPrint;

import static org.junit.jupiter.api.Assertions.*;

class StoreTest {

    @org.junit.jupiter.api.Test
    void isImported() {
        assertEquals(true, Store.isImported("Imported Chocolate"));
        assertTrue(Store.isImported("Imported Book"));
        assertTrue(Store.isImported("imported medicine"));
        assertFalse(Store.isImported("Book"));
    }

    @org.junit.jupiter.api.Test
    void calculateTaxOfProduct() {
        assertEquals(0.50, Store.roundToTwoDecimal(Store.calculateTaxOfProduct(new Product("Imported Chocolate", 1, 10.00), 5)));
        assertNotEquals(0.10, Store.roundToTwoDecimal(Store.calculateTaxOfProduct(new Product("Chocolate", 3, 10.00), 5)));
    }

    @org.junit.jupiter.api.Test
    void roundToTwoDecimal() {
        assertEquals(0.55, Store.roundToTwoDecimal(0.54312));
        assertNotEquals(0.60,Store.roundToTwoDecimal(0.55552));
    }

    @org.junit.jupiter.api.Test
    void calculateTotalAmount() {
        Store store = new Store();
        store.products.add(new Product("Chocolate",1, 10.00));
        store.products.add(new Product("book",2,20.00));
        assertEquals(50.00,Store.calculateTotalAmount(store));
        store.products.remove(0);
        store.products.add(new Product("medicine" , 1, 12.5));
        assertEquals(52.5,Store.calculateTotalAmount(store));
        store.products.remove(1);
        assertNotEquals(15,Store.calculateTotalAmount(store));
    }

}