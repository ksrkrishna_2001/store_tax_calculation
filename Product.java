package StoreReceiptPrint;

import lombok.Data;

/**
 * <h1>Product class</h1>
 * This product class is used to create an object for the product with product name, quantity and price.
 */

@Data
public class Product {
    private String name;
    private int quantity;
    private double price;

    public Product(String name, int quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

}
